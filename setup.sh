#!/bin/bash

git submodule update --init -- setup
branch=$(git config -f .gitmodules --get submodule.setup.branch)
cd setup && git checkout $branch && git pull && cd .. && /usr/bin/env python3 ./setup/setup.py $@
