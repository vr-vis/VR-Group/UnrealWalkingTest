git submodule update --init -- setup
FOR /F "tokens=*" %%a in ('git config -f .gitmodules --get submodule.setup.branch') do SET BRANCH=%%a
WHERE py
IF %ERRORLEVEL% NEQ 0 (SET PYTHON_COMMAND=python) ELSE (FOR /f "delims=" %%i in ('py -3 -c "import sys; print(sys.executable)"') do SET PYTHON_COMMAND=%%i)
ECHO %PYTHON_COMMAND%
cd setup && git checkout %BRANCH% && git pull && cd .. && "%PYTHON_COMMAND%" .\setup\setup.py %*
